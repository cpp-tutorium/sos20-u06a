#pragma once



class cSchachfeld
{
private:
	char xAchse;
	char yAchse;

	cSchachfeld* prev;
public:
	cSchachfeld(char x, char y, cSchachfeld* prev_in = nullptr);
	~cSchachfeld();

	cSchachfeld* springerzug(char x, char y);
	bool isOnC1();

private:
	static bool belegteFelder(char x, char y);
};

