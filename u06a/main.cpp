#include "cSchachfeld.h"
#include <iostream>


int main()
{
	cSchachfeld* currentPosition = new cSchachfeld('e', 6);

	char input[8];

	std::cout << "Startfeld ist e6." << std::endl;

	while (!currentPosition->isOnC1())
	{
		memset(&input, 0, sizeof(input));
		std::cout << "Bitte neue position angeben: ";
		std::cin >> input;
		currentPosition = currentPosition->springerzug(input[0], char(input[1]) - '0');
	}

	delete currentPosition;

	return 0;
}
