#include "cSchachfeld.h"
#include <iostream>
#include <cmath>

cSchachfeld::cSchachfeld(char x, char y, cSchachfeld* prev_in)
	: xAchse(x), yAchse(y), prev(prev_in)
{
	char x_lower = char(tolower(x));
	if (!(x_lower >= 'a' && x_lower <= 'h' && y >= 1 && y <= 8))
	{
		xAchse = 'a';
		yAchse = 1;
	}
}

cSchachfeld::~cSchachfeld()
{
	delete prev;
	std::cout << "Destruktor: " << xAchse << +yAchse << std::endl;
}

cSchachfeld* cSchachfeld::springerzug(char x, char y)
{
	char x_lower = char(tolower(x));

	if (!(x_lower >= 'a' && x_lower <= 'h' && y >= 1 && y <= 8))
	{
		std::cout << "Ungueltige angabe. Aktuelle position ist: " << xAchse << +yAchse << std::endl;
		return this;
	}
	
	char xDiff = char(abs(xAchse - x_lower));
	char yDiff = char(abs(yAchse - y));

	if (((xDiff == 1 && yDiff == 2) || (xDiff == 2 && yDiff == 1)) && !belegteFelder(x_lower, y))
	{
		std::cout << "Neue position ist: " << x_lower << +y << std::endl;
		return new cSchachfeld(x_lower, y, this);
	}

	std::cout << "Ungueltige angabe. Aktuelle position ist: " << xAchse << +yAchse << std::endl;
	return this;
}

bool cSchachfeld::isOnC1()
{
	return xAchse == 'c' && yAchse == 1;
}

// return true wenn belegt
bool cSchachfeld::belegteFelder(char x, char y)
{
	return (x == 'g' && y == 1) || 
		((x == 'a' || x == 'f') && y == 3) || 
		((x == 'd' || x == 'e' || x == 'h') && y == 8);
}
